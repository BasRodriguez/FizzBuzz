package cl.ubb.kata.fizzBuzz;

public class Juego {

	public int retornar1(int n){
		return 1;
	}
	
	public int retornar2(int n){
		return 2;
	}
	
	public String Fizz(int n) throws FizzBuzzException{
	 if(n%3!=0)
		 throw new FizzBuzzException();
	 else return "Fizz"; 
	}
	
	public int retornar4(int n){
		return 4;
	}
	
	public String Buzz(int n) throws FizzBuzzException{
		 if(n%5!=0)
			 throw new FizzBuzzException();
		 else return "Buzz"; 
		}
	
	public String FizzBuzz(int n) throws FizzBuzzException{
		 if(n%5!=0 && n%3!=0)
			 throw new FizzBuzzException();
		 else 
			 if(n%5==0 && n%3==0) return "FizzBuzz";
		return null; 
		}
}
