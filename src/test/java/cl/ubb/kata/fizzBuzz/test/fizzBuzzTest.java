package cl.ubb.kata.fizzBuzz.test;

import static org.junit.Assert.*;

import org.junit.Test;

import cl.ubb.kata.fizzBuzz.FizzBuzzException;
import cl.ubb.kata.fizzBuzz.Juego;


public class fizzBuzzTest {

	@Test
	public void juegoRecibeUnoDevuelveUno() throws FizzBuzzException{
		Juego juego=new Juego();
		
		
		assertEquals(1, juego.retornar1(1));
	}
	
	@Test
	public void juegoRecibeDosDevuelveDos() throws FizzBuzzException {
		Juego juego=new Juego();
		
		
		assertEquals(2, juego.retornar2(2));
	}
	
	@Test 
	public void juegoRecibeTresDevuelveFizz() throws FizzBuzzException {
		Juego juego=new Juego();
		
		assertEquals("Fizz", juego.Fizz(3));
			
	}
	
	@Test
	public void juegoRecibeCuatroDevuelveCuatro() throws FizzBuzzException{
		Juego juego=new Juego();
		
		assertEquals(4, juego.retornar4(4));
	}
	
	@Test
	public void juegoRecibeCincoDevuelveCinco() throws FizzBuzzException{
		Juego juego=new Juego();
		
		assertEquals("Buzz", juego.Buzz(5));
	}

	

}
